import sys
from pathlib import Path
import tools
import ttkbootstrap as ttk
from ttkbootstrap.constants import *

from comm.readIni import read_section
from comm.readJson import read_json

sys_config = read_section('system')
top_menu = read_json(r'menu.json')

class menuFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.root = master
        self.top_frame()
        self.nb = ttk.Notebook(self.root)
        self.table_menu()


    def top_frame(self):
        btn_group = ttk.Label(master=self.root, font="-size 24 -weight bold")
        btn_group.pack(fill=X, side=TOP, padx=10)
        # 分类
        self.add_menu(btn_group, top_menu.keys(), LEFT)

        # 分割线
        ttk.Separator(self.root).pack(fill=X, padx=10)

        # theme
        style = ttk.Style()
        theme_menu = ttk.Menu(btn_group)
        for index, name in enumerate(style.theme_names()):
            theme_menu.add_radiobutton(label=name, value=index, command=lambda theme_name=name :style.theme_use(theme_name))
        ttk.Menubutton(master=btn_group, text="主题", menu=theme_menu, width=6, style='outline-toolbutton')\
            .pack(fill=X, side=RIGHT, padx=10)


    def add_menu(self, master, menus, side=LEFT):
        # 添加分类 top 菜单
        for name in menus:
            tmb = ttk.Button(master, text=name, command=lambda n=name:self.show_table_menu(n), style="link")
            tmb.pack(side=side, fill=X)


    def table_menu(self, menu=next(iter(top_menu))):
        for name,frame in top_menu[menu].items():
            try:
                __frame = self.tab_frame(self.nb)
                mod_str,_seq, class_str = frame.rpartition('.')
                getattr(sys.modules[f'tools'], class_str)(__frame)
                self.nb.add(__frame, text=name)
            except:
                pass
        self.nb.pack(side=TOP, padx=15, pady=15, ipadx=5, ipady=5, expand=YES, fill=BOTH)

    def tab_frame(self, parent):
        frame = ttk.Frame(parent)
        frame.pack()
        return frame

    def show_table_menu(self, menu):
        """显示所选页面，隐藏上一个的页面"""
        for tab in self.nb.tabs():
            self.nb.deletecommand(tab)
        self.table_menu(menu)
