import ttkbootstrap as ttk
from tkinter.constants import *

from frame.menuFrame import menuFrame


class baseFrame(ttk.Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.root = master
        menu = menuFrame(self.root)
        menu.pack(side=LEFT, fill=BOTH, expand=YES)


