import pandas as pd
from ttkbootstrap.constants import *
from tinui.TinUI import BasicTinUI


def read_csv(file) -> pd.DataFrame:
    return pd.read_csv(file, encoding='gbk', header=None)


def table_frame(root, data) -> BasicTinUI:
    """Table 样式"""
    b = BasicTinUI(root)
    b.pack(fill=BOTH, expand=True)
    b.add_table((10, 10), data=data, minwidth=265, maxwidth=10000)
    return b


def create_table(root, file) -> BasicTinUI:
    """创建 Table"""
    data = read_csv(file).values.tolist()
    table = table_frame(root, data)
    return table
