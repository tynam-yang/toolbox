import ttkbootstrap as ttk
from ttkbootstrap.constants import *


class About(ttk.Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.root = master
        ttk.Label(self.root, text="关于我们",font="TkFixedFont 28").pack()
        text = ttk.Text(self.root)
        text.pack(fill=BOTH, expand=True)

        text.insert(END, '一名有志青年\n')
        text.insert(END, '可堪孤馆闭春寒，杜鹃声里斜阳暮\n')
