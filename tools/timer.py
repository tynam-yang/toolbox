import ttkbootstrap as ttk
from ttkbootstrap.constants import *


class Timer(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.running = ttk.BooleanVar(value=False)
        self.afterid = ttk.StringVar()
        self.elapsed = ttk.IntVar()
        self.stopwatch_text = ttk.StringVar(value="00:00:00")

        self.container(master)

    def container(self, root):
        frame = ttk.Frame(master=root, padding=2)
        self.create_stopwatch_label(frame)
        self.create_stopwatch_controls(frame)
        frame.pack(fill=BOTH, expand=YES)

    def create_stopwatch_label(self,frame):
        """Create the stopwatch number display"""
        lbl = ttk.Label(
            master=frame,
            font="-size 32",
            anchor=CENTER,
            textvariable=self.stopwatch_text,
        )
        lbl.pack(side=TOP, fill=X, padx=60, pady=20)

    def create_stopwatch_controls(self,frame):
        """Create the control frame with buttons"""
        container = ttk.Frame(frame, padding=10)
        container.pack()
        self.buttons = []
        self.buttons.append(
            ttk.Button(
                master=container,
                text="开始",
                width=10,
                style=INFO,
                command=self.on_toggle,
            )
        )
        self.buttons.append(
            ttk.Button(
                master=container,
                text="重置",
                width=10,
                style=SUCCESS,
                command=self.on_reset,
            )
        )
        for button in self.buttons:
            button.pack(side=LEFT, fill=X, expand=YES, pady=10, padx=5)

    def on_toggle(self):
        """Toggle the start and pause button."""
        button = self.buttons[0]
        if self.running.get():
            self.pause()
            self.running.set(False)
            button.configure(text="开始")
        else:
            self.start()
            self.running.set(True)
            button.configure(text="停止")


    def on_reset(self):
        """Reset the stopwatch number display."""
        self.elapsed.set(0)
        self.stopwatch_text.set("00:00:00")

    def start(self):
        """Start the stopwatch and update the display."""
        self.afterid.set(self.after(1, self.increment))

    def pause(self):
        """Pause the stopwatch"""
        self.after_cancel(self.afterid.get())

    def increment(self):
        """Increment the stopwatch value. This method continues to
        schedule itself every 1 second until stopped or paused."""
        current = self.elapsed.get() + 1
        self.elapsed.set(current)
        formatted = "{:02d}:{:02d}:{:02d}".format(
            (current // 100) // 60, (current // 100) % 60, (current % 100)
        )
        self.stopwatch_text.set(formatted)
        self.afterid.set(self.after(100, self.increment))

