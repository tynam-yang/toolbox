import ttkbootstrap as ttk
from ttkbootstrap.constants import *


class HistoryVersion(ttk.Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.root = master
        ttk.Label(self.root, text="历史版本",font="TkFixedFont 28").pack()
        text = ttk.Text(self.root)
        text.pack(fill=BOTH, expand=True)
        
        text.insert(END, '1.0\n')
        text.insert(END, '时间：2023年12月1日\n')
        text.insert(END, '初始版本发布，可用内容有：计算器、计时器\n')
