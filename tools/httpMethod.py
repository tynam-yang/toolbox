import ttkbootstrap as ttk

from frame.TableFrame import create_table


class HttpMethod(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.file = 'assets/http_method.csv'
        self.container(master)

    def container(self, root):
        create_table(root, self.file)
