import ttkbootstrap as ttk
from ttkbootstrap.constants import *

from frame.baseFrame import baseFrame
from comm.readIni import *

tool_box_icon = 'assets\\tool-box.png'
sys_config = read_section('system')

if __name__ == "__main__":
    app = ttk.Window(title="工具盒子",
                     minsize=(850, 600),
                     iconphoto=tool_box_icon,
                     themename=sys_config['theme']
                     )

    # 设置坐标位置，居中显示
    screenwidth = app.winfo_screenwidth()
    screenheight = app.winfo_screenheight()
    width = int(sys_config['width'])
    height = int(sys_config['height'])
    geometry = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
    app.geometry(geometry)

    bf = baseFrame(app)
    bf.pack(side=LEFT, fill=BOTH, expand=YES)

    app.mainloop()
