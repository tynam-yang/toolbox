##   :bangbang:  **当前仓库已弃用，使用 Dear pyGUI 重新开发。仓库地址：[https://gitee.com/tynam-yang/rookierun](https://gitee.com/tynam-yang/rookierun)** 

# 工具箱

#### 介绍
常用的工具集合

演示如下：
![群二维码.png](img/功能演示.gif)


#### 安装教程

链接：https://pan.baidu.com/s/1Bspp3peWcuo6s_eSTB85AQ?pwd=tyna 
提取码：tyna

#### 使用说明

所需 python 版本：3.11+

1.  下载代码，安装依赖包。
```shell
pip install package -i https://pypi.mirrors.ustc.edu.cn/simple/   ## 使用中科大镜像源
pip install -r requirement.txt   ## 安装脚本里列出的库
```
2. 运行 `__mian__.py` 文件。

```shell
python __mian__.py
```

#### 参与贡献

扫码进群参与，共同建设：
![群二维码.png](img/群二维码.png)

##### 步骤

1.  Fork 本仓库
2.  新建 Feat_Toolbox_{name} 分支
3.  提交代码
4.  新建 Pull Request

##### 要求

1. `./tool/` 下创建一个新的工具文件，例如 calculator.py。 
2. 添加类，继承 `ttk.Frame`，并在 Frame 中实现新增的工具样式和逻辑。如下代码：
    ```PYTHON
    class Calculator(ttk.Frame):
        def __init__(self, master, **kwargs):
            super().__init__(master, **kwargs)
            self.root = master

            self.container(self.root)
   
        def container(self, root):
            """
                页面个各种组件，及计算逻辑
                root：父组件
            """
            pass
   ```
3. 引入工具类。在 `./tool/__init__.py` 下引入添加的新工具类，例如 Calculator：

```python
from tools.calculator import Calculator
```

4. 注册组件。在 `menu.json` 对应的分类下添加新增的工具。例如添加 calculator 工具，第一层 `key` 为工具分类；第二层 `key` 为工具名；
第二层 `value` 为工具对应的类。例如 `Calculator` 类，就为 `Calculator`：
    
   ```json
   {
       "工具": {
           "计算器":"Calculator"
       }
   }
   ```

5. 新工具开发完成后，将依赖包保存在 `requirement.txt` 文件。
   ```shell
   pip list --format=freeze >requirement.txt
   ```
6. 重新生成单机应用程序。
   ```shell
   pyinstaller toolbox.spec 
   ```
7. 将 `menu.json`、 `config.ini` 和 `assets` 文件复制到与打包后的 `toolbox.exe` 同一个目录下。

