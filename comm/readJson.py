import json


def read_json(file=r'menu.json'):
    with open(file, "r", encoding="utf-8") as f:
        data = json.load(f)
    return data
