import configparser


def get_setions(file='config.ini'):
    cf = configparser.ConfigParser()
    cf.read(file)
    return cf.sections()


def get_option(section, option, file='config.ini'):
    cf = configparser.ConfigParser()
    cf.read(file)
    return cf[section][option]


def read_section(section, file='config.ini'):
    cf = configparser.ConfigParser()
    cf.read(file)
    return cf[section]
